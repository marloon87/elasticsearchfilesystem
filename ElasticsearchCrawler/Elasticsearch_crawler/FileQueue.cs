﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Serilog;

namespace ElasticsearchCrawler.Core
{
    public class FileQueue
    {
        private ConcurrentQueue<string> concurentFileQueueItems;
        public Queue<string> FileQueueItems { get; set; }
        public int ProcessedFiles { get; set; }

        public FileQueue()
        {
        }

        public void SetFileQueueItems(Queue<string> fileQueueItems)
        {
            this.FileQueueItems = fileQueueItems;
            if (FileQueueItems == null)
            {
                Log.Logger.Error("File Queue cannot be null! Closing crawler with status -1.");
                System.Environment.Exit(-1);
            }
        }

        private void PopulateConcurentQueue()
        {
            try
            {
                this.concurentFileQueueItems = new ConcurrentQueue<string>(FileQueueItems);
            }
            catch (NullReferenceException ex)
            {
                Log.Logger.Error("File Queue cannot be empty! Closing crawler service {0}", ex);
                System.Environment.Exit(-1);
            }
        }

        public void OperateOnQueue()
        {
                string rawFilePath = "";
                PopulateConcurentQueue();
                while (!concurentFileQueueItems.IsEmpty)
                {
                    ReadFile fileRead = new ReadFile();
                    Task t1 = Task.Factory.StartNew(() =>
                    {
                        Log.Logger.Information("Removing object from Queue on thread {0} ", (Thread.CurrentThread.ManagedThreadId).ToString());
                        concurentFileQueueItems.TryDequeue(out rawFilePath);
                        fileRead.SetRawFilePath(rawFilePath);
                    });
                    Task t2 = Task.Factory.StartNew(() =>
                    {
                        Log.Logger.Information("File {1} processing on thread {0} ", (Thread.CurrentThread.ManagedThreadId).ToString(), rawFilePath);
                        fileRead.SendFileforProcessing();
                        if (fileRead.operationOnFileSuccessful)
                        {
                            ProcessedFiles++;
                        }
                    });
                    try
                    {
                        Task.WaitAll(t1, t2);
                    }
                    catch (AggregateException ex)
                    {
                        Log.Logger.Error("Something went seriously wrong with the threads!", ex.Flatten().Message);
                    }
                }
        }
    }
}
