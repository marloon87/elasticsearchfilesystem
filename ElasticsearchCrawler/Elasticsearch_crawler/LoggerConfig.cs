﻿using System;
using Serilog;
using System.Configuration;

namespace ElasticsearchCrawler.Core
{
    public class LoggerConfig
    {
        private string loggerDirectory = "";
        private string loggerFileName = "";

        public LoggerConfig()
        {
            SetUpLoggerFilePath();
            Log.Logger = new LoggerConfiguration()
                .WriteTo.File(loggerFileName)
                .CreateLogger();
        }
        
        private void SetUpLoggerFilePath ()
        {
            this.loggerDirectory = ConfigurationManager.AppSettings["LoggerFileDirectory"];
            this.loggerFileName = ConfigurationManager.AppSettings["LoggerFileName"];
            this.loggerFileName = string.Concat(this.loggerDirectory, this.loggerFileName);
        }

    }
}
