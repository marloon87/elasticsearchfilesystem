﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;
using System.IO;

namespace ElasticsearchCrawler.Core
{
    public class FileSystemDirectory
    {
        public String FileDirectory { get; set; }
        private Queue<string> enumeratedFileQueue = new Queue<string>();

        public FileSystemDirectory()
        {
        }

        public FileSystemDirectory(String fileDirectory)
        {
            this.FileDirectory = fileDirectory;
            EnumerateFiles();
        }

        public Queue<string> GetFileQueue()
        {
            return this.enumeratedFileQueue;
        }

        public void SetFileQueue(Queue<string> enumeratedFileQueue)
        {
            this.enumeratedFileQueue = enumeratedFileQueue;
        }

        private bool IsDirectory()
        {
            bool isDir = false;
            try
            {
                isDir = (File.GetAttributes(FileDirectory) & FileAttributes.Directory) == FileAttributes.Directory;
            }
            catch (IOException IOex)
            {
                Log.Error("I/O Exception: {0}", IOex);
            }
            return isDir;
        }

        private void EnumerateFiles()
        {
            if (IsDirectory())
            {
                try
                {
                    enumeratedFileQueue = new Queue<string>(Directory.EnumerateFiles(FileDirectory, "*.*", SearchOption.AllDirectories));
                }
                catch (UnauthorizedAccessException UAEx)
                {
                    Log.Error("Cannot access files, unathorized: {0}. Trying recursive method..", UAEx);
                    ProcessDirectory(this.FileDirectory);
                }
                catch (PathTooLongException PathEx)
                {
                    Log.Error("Path to file too long: {0}", PathEx);
                }
            }
            else
            {
                Log.Error("Provided file path is not a valid directory. Closing Crawler with status code -1.");
                System.Environment.Exit(-1);
            }
        }
        
        private void ProcessDirectory(string fileDirectory)
        {
            try
            {
                string[] fileEntries = Directory.GetFiles(fileDirectory);
                foreach (string fileName in fileEntries)
                {
                    if (!String.IsNullOrEmpty(fileName))
                    {
                        this.enumeratedFileQueue.Enqueue(fileName);
                    }
                }

                string[] subdirectoryEntries = Directory.GetDirectories(fileDirectory);
                foreach (string subdirectory in subdirectoryEntries)
                {
                    ProcessDirectory(subdirectory);
                }
            }
            catch (UnauthorizedAccessException e)
            {
                Log.Error("Cannot read file: {0}", e);
            }
        }
    }
}
