﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using ElasticSearchCrawler.API;
using Serilog;

namespace ElasticsearchCrawler.Core
{
    public class PublishDocument
    {
        public PublishDocument()
        {
        }

        public void PublishFileToElasticsearch(string documentContent, string rawFilePath)
        {
                Log.Logger.Information("Service started");
                try
                {
                    ElasticSearchPublisher publisher = new ElasticSearchPublisher();
                    publisher.PublishDocumentWithPost(documentContent, rawFilePath);
                    Log.Logger.Information("File processed and sent to Elasticsearch");
                }
                catch (CommunicationException cex)
                {
                    Log.Logger.Error("Communication failed! {0}", cex);
                }
                Log.Logger.Information("Service closed");
        }
    }
}