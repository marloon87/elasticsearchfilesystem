﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;

namespace ElasticsearchCrawler.Core
{
    class Program
    {
        static void Main(string[] args)
        {
            var log = new LoggerConfig();
            string fileDirectory = "D:\\Documents";
            /* TO DO
             * Directory will be taken from API */

            var runProgram = new FileSystemDirectory(fileDirectory);
            var fileQueue = new FileQueue();
            fileQueue.SetFileQueueItems(runProgram.GetFileQueue());
            Log.Logger.Information("Total files to be processed: {0}", fileQueue.FileQueueItems.Count.ToString());
            fileQueue.OperateOnQueue();
            Log.Logger.Information("Total files processed: {0}", fileQueue.ProcessedFiles);
        }
    }
}
