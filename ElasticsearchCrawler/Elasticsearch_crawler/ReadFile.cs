﻿using System;
using System.Threading.Tasks;
using Serilog;
using System.IO;
using System.Configuration;
using System.Text.RegularExpressions;
using ElasticsearchCrawler;

namespace ElasticsearchCrawler.Core
{
    public class ReadFile : FileQueue
    {
        private string rawFilePath = "";
        public bool operationOnFileSuccessful = false;

        public ReadFile()
        {
        }

        public void SendFileforProcessing()
        {
            string extensionsForUse = ConfigurationManager.AppSettings["FileExtensions"];
            if (rawFilePath != "" && Regex.Match(rawFilePath, extensionsForUse).Success)
            {
                Log.Logger.Information("File {0} sent for processing", rawFilePath);
                StreamReadFile();
                this.operationOnFileSuccessful = true;
                Log.Logger.Information("File {0} processed", rawFilePath);
            }
            else
            {
                Log.Logger.Information("File {0} does not meet requirements. Skipped.", rawFilePath);
            }
        }

        public async void StreamReadFile()
        {
            string doc = "";
            try
            {
                using (StreamReader sr = new StreamReader(this.rawFilePath))
                {
                    doc = await sr.ReadToEndAsync();
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Warning("File {0} could not be read. Exception: {1}", this.rawFilePath, ex.Message);
            }
            PublishDocument pd = new PublishDocument();
            pd.PublishFileToElasticsearch(doc, this.rawFilePath);
        }

        public void SetRawFilePath(string rawFilePath)
        {
            this.rawFilePath = rawFilePath;
        }
    }
}
