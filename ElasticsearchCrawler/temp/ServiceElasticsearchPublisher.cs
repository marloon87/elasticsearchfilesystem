﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ElasticsearchCrawlerPublisher
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class ElasticsearchCrawlerPublisherService : ElasticsearchCrawlerPublisherService
    {

        private string documentContent = "";
        private bool retrievalStatus = false;
        private bool sendingStatus = false;

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public bool RetrieveDocument(string documentContent)
        {
            this.documentContent = documentContent;
            return this.retrievalStatus;
        }

        public bool SendDocumentToElasticsearch()
        {
            TransportDocument();
            return this.sendingStatus;
        }

        private void TransportDocument()
        {
            Log.Logger.Information("File Content :{0} ", documentContent);
        }
    }
}
