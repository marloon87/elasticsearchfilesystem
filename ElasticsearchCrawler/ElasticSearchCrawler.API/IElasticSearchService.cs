﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ElasticSearchCrawler.API
{
    [ServiceContract]
    public interface IElasticSearchPublisher
    {

        [OperationContract]
        [WebGet]
        void PublishDocumentWithGet(string documentContent, string rawFilePath);

        [OperationContract]
        [WebInvoke]
        void PublishDocumentWithPost(string documentContent, string rawFilePath);

    }

}
