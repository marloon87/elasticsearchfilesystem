﻿using System;
using Serilog;
using System.Configuration;

namespace ElasticSearchCrawler.API
{
    public class PublisherLoggerConfig
    {
        private string loggerDirectory = "";
        private string loggerFileName = "";

        public PublisherLoggerConfig()
        {
            SetUpLoggerFilePath();
            Log.Logger = new LoggerConfiguration()
                .WriteTo.File(loggerFileName)
                .CreateLogger();
        }
        
        private void SetUpLoggerFilePath ()
        {
           //this.loggerDirectory = ConfigurationManager.AppSettings["LoggerFileDirectory"];
           //this.loggerFileName = ConfigurationManager.AppSettings["LoggerFileName"];
           this.loggerDirectory = "..\\..\\ElasticsearchPublisherDocuments\\";
           this.loggerFileName = "ElasticsearchPublisher.log";
           this.loggerFileName = string.Concat(this.loggerDirectory, this.loggerFileName);
        }

    }
}
