﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ElasticSearchCrawler.API
{
    public class ElasticSearchPublisher : IElasticSearchPublisher
    {
        public void PublishDocumentWithGet(string documentContent, string rawFilePath)
        {
            ElasticSearchInterface objSearch = new ElasticSearchInterface();
            DataModel dm = new DataModel();
            objSearch.AddNewIndex(new DataModel(documentContent, rawFilePath));

        }

        public void PublishDocumentWithPost(string documentContent, string rawFilePath)
        {
            ElasticSearchInterface objSearch = new ElasticSearchInterface();
            DataModel dm = new DataModel();
            objSearch.AddNewIndex(new DataModel(documentContent, rawFilePath));
        }
    }
}
