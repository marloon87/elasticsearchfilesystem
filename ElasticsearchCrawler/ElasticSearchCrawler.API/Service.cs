﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Description;
using Serilog;

namespace ElasticSearchCrawler.API
{
    public class Service
    {
        static void Main(string[] args)
        {
            string baseAddress = "http://localhost:8000/ElasticSearchPublisher";
            WebServiceHost host = new WebServiceHost(typeof(Service), new Uri(baseAddress));
            ServiceEndpoint ep = host.AddServiceEndpoint(typeof(IElasticSearchPublisher), new WebHttpBinding(), "");
            host.Open();
            Log.Logger.Information("Service started");
            Console.ReadLine();
            host.Close();
            Log.Logger.Information("Service closed");
        }
    }
}