﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using Elasticsearch.Net;
using ElasticsearchWeb.API.Models;

namespace ElasticsearchWeb.API.Controllers
{
    public class DataModelController : ApiController
    {
        DataModel documents = new DataModel();

        public DataModel GetResults (string webQuery)
        {
            var settings = new ConnectionConfiguration(new Uri("http://localhost:9200")).RequestTimeout(TimeSpan.FromMinutes(2));
            var lowlevelClient = new ElasticLowLevelClient(settings);
            var searchQuery = @"{
                    ""from"": 0,
                    ""size"": 10,
                    ""query"": {
                    ""match"": {
                          ""filePath"": """ + webQuery + @"""
                            }
                         }
                    }";

            var searchResponse = lowlevelClient.Search<StringResponse>("fileindex", searchQuery);
            if (!searchResponse.Success) {
                documents.Content = "Not found";
                return documents;
            } 
            documents.Content = searchResponse.Body;
            return documents;
        }

        public DataModel GetAllData()
        {
            var settings = new ConnectionConfiguration(new Uri("http://localhost:9200")).RequestTimeout(TimeSpan.FromMinutes(2));
            var lowlevelClient = new ElasticLowLevelClient(settings);
            var searchResponse = lowlevelClient.Search<StringResponse>("fileindex", @"
                {
                    ""from"": 1,
                    ""size"": 5,
                    ""query"": {
                        ""match_all"": {
                        }
                    }
                }");
            documents.Content = searchResponse.Body;
            return documents;
        }
    }
}
