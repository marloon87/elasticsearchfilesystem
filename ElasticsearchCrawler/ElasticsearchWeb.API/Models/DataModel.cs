﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticsearchWeb.API.Models
{
    public class DataModel
    {
        public string FilePath { get; set; }
        public string Content { get; set; }
    }
}