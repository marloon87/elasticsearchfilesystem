﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElasticSearchCrawler.API
{
    public class DataModel
    {
        public string Content { get; set; }
        public string FilePath { get; set; }
        public DataModel ()
        {

        }
        public DataModel (string content, string filepath)
        {
            this.Content = content;
            this.FilePath = filepath;
        }

    }
}