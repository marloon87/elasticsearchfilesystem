﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nest;
using Serilog;

namespace ElasticSearchCrawler.API
{
    public class ElasticSearchInterface
    {
        public ElasticClient client = null;
        public ElasticSearchInterface ()
        {
            var uri = new Uri("http://localhost:9200");
            try
            {
                var settings = new ConnectionSettings(uri).DefaultIndex("fileindex");
                client = new ElasticClient(settings);
                Log.Logger.Information("Connection with Elasticsearch established");
            }
            catch (Exception Ex)
            {
                Log.Logger.Error("Could not connect to Elasticsearch. {0}", Ex);
            }

        }

        public void AddNewIndex(DataModel model)
        {
            try
            {
                client.IndexDocument<DataModel>(model);
            }
            catch (Exception ex)
            {
                Log.Logger.Error("Adding document to database failed, {0}", ex);
            }
        }
    }
}